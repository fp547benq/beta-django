from django.db import models
from django.utils import timezone
from django.shortcuts import reverse

class Category(models.Model):
    title = models.CharField('Имя категории', max_length=255)
    image = models.ImageField('Картинка', blank=True, null=False, upload_to="categories/")
    slug = models.SlugField('Ссылка', unique=True)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'slug':self.slug})

    def __str__(self):
        return self.title

class Post(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    Image = models.ImageField("Картинка", blank=True, null=True, upload_to="posts/")
    slug = models.SlugField("Ссылка", unique=True)
    summary = models.TextField("Краткое описание")
    text = models.TextField("Текст")
    сategory = models.ForeignKey(Category, null=True, on_delete = models.CASCADE, verbose_name = "Категория")
    date = models.DateTimeField("Дата", default=timezone.now)

    class Meta:
        verbose_name="Новость"
        verbose_name_plural="Новости"

    def get_absolute_url(self):
        return reverse('post_detail_url', kwargs={'slug':self.slug})


    def __str__(self):
        return self.title

# Create your models here.
