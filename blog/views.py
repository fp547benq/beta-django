from django.shortcuts import render, redirect
from .models import Post, Category
from django.db.models import Q
from .forms import RegisterForm

def index(request):
    return render(request, 'blog/index.html')
def blog(request):
    news = Post.objects.order_by('-date')
    return render(request, 'blog/blog.html', {'news':news})
def search(request):
    query = request.GET.get('search')
    search_obj=Post.objects.filter(
        Q(title__icontains = query) | Q(summary__icontains = query)
    )
    context = {'query' : query, 'search_obj': search_obj}
    return render(request, 'blog/search.html', context) 

def post_detail(request, slug):
    post = Post.objects.get(slug__exact = slug)
    return render(request, 'blog/post_detail.html', {'post':post})

def category_detail(request, slug):
    category = Category.objects.get(slug__exact = slug)
    return render(request, 'blog/category_detail.html', {'category':category})

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = RegisterForm()
    return render(request, 'blog/register.html', {'form':form})



# Create your views here.
